regression.evaluation package
=============================

Submodules
----------

regression.evaluation.errors module
-----------------------------------

.. automodule:: regression.evaluation.errors
   :members:
   :undoc-members:
   :show-inheritance:

regression.evaluation.evaluators module
---------------------------------------

.. automodule:: regression.evaluation.evaluators
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: regression.evaluation
   :members:
   :undoc-members:
   :show-inheritance:
