regression package
==================


Subpackages
-----------

.. toctree::
   :maxdepth: 4

   regression.evaluation
   regression.persistence
   regression.testing
   regression.training
   regression.visualization

Submodules
----------
regression.errors module
------------------------

.. automodule:: regression.errors
   :members:
   :undoc-members:
   :show-inheritance:

regression.regression module
----------------------------

.. automodule:: regression.regression
   :members:
   :undoc-members:
   :show-inheritance:

