test package
============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   test.regression

Module contents
---------------

.. automodule:: test
   :members:
   :undoc-members:
   :show-inheritance:
