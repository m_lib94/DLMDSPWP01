import unittest

from regression.visualization import VisualizerFactory
from regression.visualization.visualization import TrainingVisualizer, FunctionsVisualizer


class VisualizationFactoryTest(unittest.TestCase):
    """
    Test class for visualizer factory
    """

    def test_correct_instantiation(self):
        """
        Test correct instantiation
        :return:
        """
        visualizer = VisualizerFactory.get_instance().create_visualizer("training")

        self.assertIsInstance(visualizer, TrainingVisualizer)

    def test_adding_custom_visualizer(self):
        """
        Test adding custom visualizer
        :return:
        """
        visualizerFactory = VisualizerFactory.get_instance()
        visualizerFactory.register_visualizer('test_visualizer', FunctionsVisualizer)

        visualizer = visualizerFactory.create_visualizer('test_visualizer')

        self.assertIsInstance(visualizer, FunctionsVisualizer)

    def test_not_existing_visualizer(self):
        """
        Try to receive non existing visualizer
        :return:
        """
        with self.assertRaises(Exception):
            visualizer = VisualizerFactory.get_instance().create_visualizer("not_existing")


if __name__ == '__main__':
    unittest.main()
