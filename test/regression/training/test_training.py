import unittest

import pandas

from regression.training import ModelTrainerFactory
from regression.training.training import RegressionModelTrainer


class TrainingFactoryTest(unittest.TestCase):
    """
    Test class for training factory
    """

    def test_correct_instantiation(self):
        """
        Test correct instantiation
        :return:
        """
        trainer = ModelTrainerFactory.get_instance().create_model_trainer("regression")

        self.assertIsInstance(trainer, RegressionModelTrainer)

    def test_adding_custom_trainer(self):
        """
        Test adding custom trainer
        :return:
        """
        trainerFactory = ModelTrainerFactory.get_instance()
        trainerFactory.register_trainer('test_trainer', RegressionModelTrainer)

        trainer = trainerFactory.create_model_trainer('test_trainer')

        self.assertIsInstance(trainer, RegressionModelTrainer)

    def test_not_existing_trainer(self):
        """
        Try to receive non existing trainer
        :return:
        """
        with self.assertRaises(Exception):
            trainer = ModelTrainerFactory.get_instance().create_model_trainer("not_existing")


class RegressionTrainingTest(unittest.TestCase):
    """
    Regression Test class.
    """
    def setUp(self) -> None:
        self._trainer = ModelTrainerFactory.get_instance().create_model_trainer("regression")
        self._training_functions = {'x': [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
                                    'y1': [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
                                    'y2': [10.1, 11, 12, 13, 14, 15, 16, 17, 18, 19],
                                    'y3': [20, 21, 22, 23, 24, 25, 26, 27, 28, 29],
                                    'y4': [30, 31, 32, 33, 34, 35, 36, 37, 38, 39]}
        self._ideal_function = {'x': [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
                                'y1': [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
                                'y2': [10, 11, 12, 13, 14, 15, 16, 17, 18, 19],
                                'y3': [20, 21, 22, 23, 24, 25, 26, 27, 28, 29],
                                'y4': [30, 31, 32, 33, 34, 35, 36, 37, 38, 39],
                                'y5': [301, 311, 321, 331, 341, 351, 361, 371, 381, 39]}

    def test_correct_calculation(self):
        """
        Test correct calculation
        :return: None
        """
        training_functions = pandas.DataFrame(self._training_functions)
        ideal_functions = pandas.DataFrame(self._ideal_function)

        result = self._trainer.train(training_functions, ideal_functions)

        self.assertEqual(list(result.get_best_fitting().keys()), ['y1', 'y2', 'y3', 'y4'])


if __name__ == '__main__':
    unittest.main()
