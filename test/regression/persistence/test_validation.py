import unittest
import pandas

from regression.persistence.errors import ValidationError
from regression.persistence.validation import TrainingDataValidator, IdealDataValidator


class TrainingValidationTest(unittest.TestCase):
    """
    Test class for validating training data.
    """

    def setUp(self):
        self._validator = TrainingDataValidator()

    def test_validation_correct(self):
        """
        Test case for validating with correct data
        :return: None
        """
        data_dict = {'x':  [1.0, 2.0, 3.0, 4.0, 5.0],
                     'y1': [1.0, 2.0, 3.0, 4.0, 5.0],
                     'y2': [1.0, 2.0, 3.0, 4.0, 5.0],
                     'y3': [1.0, 2.0, 3.0, 4.0, 5.0],
                     'y4': [1.0, 2.0, 3.0, 4.0, 5.0], }

        test_data = pandas.DataFrame(data_dict, columns=['x', 'y1', 'y2', 'y3', 'y4'])

        try:
            self._validator.validate(test_data)
        except ValidationError as e:
            self.fail("An unexpected exception occurred " + str(e.errors))

    def test_validation_wrong_column_name(self):
        """
        Test case for validating with wrong column name
        :return: None
        """
        data_dict = {'x':  [1, 2, 3, 4, 5],
                     'y1': [1, 2, 3, 4, 5],
                     'y2': [1, 2, 3, 4, 5],
                     'y3': [1, 2, 3, 4, 5],
                     'y8': [1, 2, 3, 4, 5], }

        test_data = pandas.DataFrame(data_dict, columns=['x', 'y1', 'y2', 'y3', 'y8'])

        with self.assertRaises(ValidationError):
            self._validator.validate(test_data)

    def test_validation_wrong_column_number(self):
        """
        Test case for validating with wrong column name
        :return: None
        """
        data_dict = {'x':  [1, 2, 3, 4, 5],
                     'y1': [1, 2, 3, 4, 5],
                     'y2': [1, 2, 3, 4, 5],
                     'y3': [1, 2, 3, 4, 5]}

        test_data = pandas.DataFrame(data_dict, columns=['x', 'y1', 'y2', 'y3'])

        with self.assertRaises(ValidationError):
            self._validator.validate(test_data)


class IdealDataValidatorTest(unittest.TestCase):
    """
    Test class for validating ideal data.
    """
    def setUp(self) -> None:
        self._validator = IdealDataValidator()

    def test_validation_correct(self):
        """
        Test case for validating with correct data
        :return: None
        """
        data_dict = {'x':  [1.0, 2.0, 3.0, 4.0, 5.0],
                     'y1': [1.0, 2.0, 3.0, 4.0, 5.0],
                     'y2': [1.0, 2.0, 3.0, 4.0, 5.0],
                     'y3': [1.0, 2.0, 3.0, 4.0, 5.0],
                     'y4': [1.0, 2.0, 3.0, 4.0, 5.0], }

        test_data = pandas.DataFrame(data_dict, columns=['x', 'y1', 'y2', 'y3', 'y4'])

        try:
            self._validator.validate(test_data)
        except Exception as e:
            self.fail("An unexpected exception occurred " + e.message)

    def test_validation_wrong_column_name(self):
        """
        Test case for validating with wrong column name
        :return: None
        """
        data_dict = {'x':  [1, 2, 3, 4, 5],
                     'y1': [1, 2, 3, 4, 5],
                     'y2': [1, 2, 3, 4, 5],
                     'y3': [1, 2, 3, 4, 5],
                     'y8': [1, 2, 3, 4, 5], }

        test_data = pandas.DataFrame(data_dict, columns=['x', 'y1', 'y2', 'y3', 'y8'])

        with self.assertRaises(ValidationError):
            self._validator.validate(test_data)

    def test_validation_wrong_column_number(self):
        """
        Test case for validating with wrong column number
        :return: None
        """
        data_dict = {'x':  [1, 2, 3, 4, 5],
                     'y1': [1, 2, 3, 4, 5],
                     'y2': [1, 2, 3, 4, 5],
                     'y3': [1, 2, 3, 4, 5]}

        test_data = pandas.DataFrame(data_dict, columns=['x', 'y1', 'y2', 'y3'])

        with self.assertRaises(ValidationError):
            self._validator.validate(test_data)



if __name__ == '__main__':
    unittest.main()
