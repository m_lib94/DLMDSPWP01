import unittest
import pandas

from regression.evaluation import LeastSquareEvaluator
from regression.evaluation.errors import EvaluationException


class EvaluationTest(unittest.TestCase):
    """
    Test class for evaluation
    """
    def setUp(self):
        self._evaluator = LeastSquareEvaluator()

    def test_calculation_correct(self):
        """
        Test correct calculation
        :return:
        """
        source_data = [1, 1, 1, 1, 1, 1]
        predicted_data = [1, 1, 1, 1, 1, 1]

        result = self._evaluator.evaluate(source_data, predicted_data)

        self.assertEqual(result[0], 0)
        self.assertEqual(result[1], 0)

    def test_validation_wrong_data_type(self):
        """
        Test evaluation with wrong data type
        :return:
        """
        source_data = ["A", 1, 1, 1, 1, 1]
        predicted_data = [1, 1, 1, 1, 1, 1]

        with self.assertRaises(Exception):
            result = self._evaluator.evaluate(source_data, predicted_data)

    def test_validation_data_not_compatible(self):
        """
        Test evaluation with non compatible data
        :return:
        """
        source_data = [1, 1, 1, 1, 1, 1]
        predicted_data = [1, 1, 1, 1, 1]

        with self.assertRaises(EvaluationException):
            result = self._evaluator.evaluate(source_data, predicted_data)


if __name__ == '__main__':
    unittest.main()
