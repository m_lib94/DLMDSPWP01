"""
Basic regression module.
Provides class Regression to calculate best fitting functions.
"""
from .regression import Regression
