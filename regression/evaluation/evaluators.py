import abc

from .errors import EvaluationException


class Evaluator(metaclass=abc.ABCMeta):
    """
    Abstract evaluator.
    """

    @abc.abstractmethod
    def evaluate(self, source_data: list, predicted_data: list) -> (float, float):
        pass

    @staticmethod
    def _is_data_compatible(source_data, predicted_data) -> None:
        """
        Checks if the data that needs to be evaluated is compatible in dimension
        :param source_data:
        :param predicted_data:
        :return:
        :raises:
        """
        # check if x-value of data are compatible
        if len(source_data) != len(predicted_data):
            raise EvaluationException("data is not compatible")


class LeastSquareEvaluator(Evaluator):
    """
    Calculates the least squares for two arrays of numeric values
    """

    def evaluate(self, source_data, predicted_data) -> (float, float):
        """
        Calculates the least squares for two arrays of numeric values
        :param source_data:
        :param predicted_data:
        :return The calculated least squares AND the maximum calculated deviation:
        :raises:
        """
        super()._is_data_compatible(source_data, predicted_data)
        least_squares = 0
        max_deviation = 0
        for i in (0, len(source_data) - 1):
            deviation = abs(source_data[i] - predicted_data[i])
            if deviation > max_deviation:
                max_deviation = deviation
            least_squares += deviation ** 2

        return least_squares, max_deviation
