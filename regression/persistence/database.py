import pandas
import sqlalchemy
from sqlalchemy import MetaData, literal_column, text
from regression.persistence.validation import TrainingDataValidator, IdealDataValidator, EvaluationDataValidator


class Database(object):
    """
    Database handling for SQL Database.
    Handling and creating the database instance for the regression task.
    Provides interfaces for adding and selecting data.
    """

    def __init__(self):
        """

        """
        self._db_engine = sqlalchemy.create_engine('sqlite:///regression.db')
        self._training_validator = TrainingDataValidator()
        self._ideal_validator = IdealDataValidator()
        self._eval_validator = EvaluationDataValidator()

    def get_training_functions(self):
        """

        :return:
        """
        connection = self._db_engine.connect()
        meta = MetaData()
        meta.reflect(bind=self._db_engine)
        training_table = sqlalchemy.Table("training",
                                          meta,
                                          autoload=True,
                                          autoload_with=self._db_engine)

        select_training = sqlalchemy.select([training_table])
        return pandas.read_sql(select_training, self._db_engine)

    def get_ideal_functions(self, functions=None):
        """

        :param functions:
        :return:
        :raises:
        """
        connection = self._db_engine.connect()
        meta = MetaData()
        meta.reflect(bind=self._db_engine)
        training_table = sqlalchemy.Table("ideal",
                                          meta,
                                          autoload=True,
                                          autoload_with=self._db_engine)

        if functions is None:
            select_ideal = sqlalchemy.select([training_table])
        else:
            literal_columns = [literal_column("x")]
            for func in functions:
                literal_columns.append(literal_column(func))

            select_ideal = sqlalchemy.select(literal_columns).select_from(text("ideal"))

        return pandas.read_sql(select_ideal, self._db_engine)

    def insert_training_functions(self, data):
        """

        :param data:
        :return:
        :raises:
        """
        self._training_validator.validate(data)
        data.to_sql('training',
                    con=self._db_engine,
                    if_exists='replace',
                    index=False)

    def insert_ideal_functions(self, data):
        """

        :param data:
        :return:
        :raises:
        """
        self._ideal_validator.validate(data)
        data.to_sql('ideal',
                    con=self._db_engine,
                    if_exists='replace',
                    index=False)

    def insert_evaluation(self, data):
        """
        Inserts the evaluation data
        :param data:
        :return:
        :raises:
        """
        data.to_sql('evaluation',
                    con=self._db_engine,
                    if_exists='replace',
                    index=False)

    def __del__(self):
        """
        If the object gets deleted. Dispose the database, so in case of an hard
        exit no open connection to the database will exist.
        :return:
        """
        self._db_engine.dispose()
