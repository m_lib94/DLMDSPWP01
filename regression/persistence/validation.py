import abc
from .errors import ValidationRuleError, ValidationError


class Validator(metaclass=abc.ABCMeta):
    """
    Abstract validator class.
    To handle every validator of dataframe the same this class needs to be used as base class.
    IMPORTANT: The number of column validators is used to validate the dimension of a dataframe!
    """

    # column validators
    _column_validators = []

    @abc.abstractmethod
    def validate(self, dataframe):
        """
        Abstract method fpr validation of a dataframe
        :param dataframe:
        :return:
        """
        pass

    def _check_column_number(self, dataframe):
        """
        Checks, if the dataframe has a many columns as the the defined colums
        :param dataframe:
        :return:
        """
        if len(dataframe) != len(self._column_validators):
            return False
        else:
            return True


class GenericValidator(Validator):
    """
    Generic validator.
    Takes a number of ColumnValidators to validate the structure oF a dataframe
    """

    def __init__(self, column_validators):
        """
        Inititlizes a generic ColumnValidator
        :param column_validators: A list of ColumnValidator. Order is important.
        """
        self._column_validators = column_validators

    def validate(self, dataframe):
        """
        Validates the dataframe based on the defined ColumnValidators
        :param dataframe:
        :return:
        """

        # check, if dataframe has correct dimension
        self._check_column_number(dataframe)

        # iterate over columns and use column validators
        # In this case we are safe, that we have the same number
        # of columns and column validators
        errors = []
        iterator = self._column_validators.__iter__()
        for index, column in dataframe.iteritems():
            try:
                column_error = iterator.__next__().validate(column)
                if len(column_error) > 0:
                    errors.append(column_error)
            except Exception as e:
                print(e)

        if len(errors) > 0:
            raise ValidationError(errors,
                                  "Some errors occurred during data validation")


class TrainingDataValidator(GenericValidator):
    """
    Validator class for training data.
    The training data is defined as follows:
    Column - x, dtype: Float
    Column - y1. dtype: Float
    Column - y2, dtype: Float
    Column - y3, dtype: Float
    Column - y4, dtype: Float
    """

    def __init__(self):
        """
        Initializes the validator for training data
        """
        super().__init__([
            ColumnValidator('x', [
                DTypeValidationRule("float64")
            ]),
            ColumnValidator('y1', [
                DTypeValidationRule("float64")
            ]),
            ColumnValidator('y2', [
                DTypeValidationRule("float64")
            ]),
            ColumnValidator('y3', [
                DTypeValidationRule("float64")
            ]),
            ColumnValidator('y4', [
                DTypeValidationRule("float64")
            ]),
        ])


class IdealDataValidator(GenericValidator):
    """
    Validator class for ideaL data.
    The training data is defined as follows:
    Column - x, dtype: Float
    Columns from y1 - y50, dtype: Float
    """

    def __init__(self):
        """
        Initializes the ideal data validator
        """
        validators = [ColumnValidator('x', [DTypeValidationRule("float64")])]

        for i in range(1, 51):
            validators.append(ColumnValidator('y' + str(i), [DTypeValidationRule("float64")]))

        super().__init__(validators)


class EvaluationDataValidator(GenericValidator):
    """
    Validator class for ideaL data.
    The training data is defined as follows:
    Column - x, dtype: Float
    Columns from y1 - y50, dtype: Float
    """

    def __init__(self):
        """

        """
        super().__init__([
            ColumnValidator('x', [
                DTypeValidationRule("float64")
            ]),
            ColumnValidator('y1', [
                DTypeValidationRule("float64")
            ]),
            ColumnValidator('y2', [
                DTypeValidationRule("float64")
            ]),
            ColumnValidator('y3', [
                DTypeValidationRule("float64")
            ]),
            ColumnValidator('y4', [
                DTypeValidationRule("float64")
            ]),
        ])


class ColumnValidator:

    def __init__(self, column_name, validation_rules):
        """
        Initializes a gerneric validator for a dataframe columns
        :param column_name:
        :param validation_rules:
        :return:
        """
        self._column_name = column_name
        self._validation_rules = validation_rules

    def validate(self, column):
        """

        :return:
        """
        errors = []
        if column.name != self._column_name:
            errors.append("Column name should be " + self._column_name)

        for validation_rule in self._validation_rules:
            try:
                validation_rule.validate(column)
            except ValidationRuleError as validationErr:
                errors.append(validationErr.message)

        return errors


class ValidationRule(metaclass=abc.ABCMeta):
    """
    A Validation rule checks only a column
    """

    @abc.abstractmethod
    def validate(self, column):
        """
        Abstract method
        :param column:
        :return:
        :raises:
        """
        pass


class DTypeValidationRule(ValidationRule):
    """
    Check if a Series is of specific type
    """

    def __init__(self, column_type):
        """
        Initializes the validation rule with a specific datatype
        :param column_type:
        """
        self._type = column_type

    def validate(self, column):
        """

        :param column:
        :return:
        """
        if column.dtype.name != self._type:
            raise ValidationRuleError(column.name,
                                      "Data type of column is " +
                                      column.dtype.name +
                                      " but must be " +
                                      self._type)
