class ValidationError(Exception):
    """
     Exception raised when errors during validaition of dataframe occur
        Attributes:
            column_name -- name of the column where validation fails
            message -- explanation of the validation error
    """
    def __init__(self, errors, message):
        """

        """
        self.errors = errors
        self.message = message
        super().__init__(self.message)


class ValidationRuleError(Exception):
    """
     Exception raised when errors during validation of a validation rule occur
      Attributes:
        column_name -- name of the column where validation fails
        message -- explanation of the validation error
    """
    def __init__(self, column_name, message):
        """

        :param column_name:
        :param message:
        """
        self.column_name = column_name
        self.message = message
        super().__init__(self.message)
