"""
Training module.
Provides a ModelTrainerFactory to create predefined trainer.
Possibility to overwrite and enhance existing trainer with custom ones.
"""
from .training import ModelTrainerFactory, ModelTrainer
