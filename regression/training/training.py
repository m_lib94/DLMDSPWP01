import abc
import pandas
from regression import evaluation


class TrainingResult:
    """
    Describes the result of a training process.
    """

    def __init__(self, result):
        """
        Initializes the TrainingResult
        :param result: A dataframe.
        """
        self._result = result

    def get_best_fitting(self):
        """
        Returns the results for the best fitting functions.
        :param function_name: If filled, it returns the result for the passed function
        :return:
        """
        fitting_functions = {}
        for function in self._result:
            least_squares_for_function = self._result[function]
            best_fitting = min(least_squares_for_function,
                               key=lambda k: least_squares_for_function[k][0])
            print(least_squares_for_function[best_fitting][1])
            fitting_functions[best_fitting] = least_squares_for_function[best_fitting]

        return fitting_functions

    def get_all_results(self):
        """
        Returns the whole TrainingResult with all calculated parameters.
        :return:
        """
        return self._result


class ModelTrainer(metaclass=abc.ABCMeta):
    """
    Abstract base class for all model trainers.
    """

    @abc.abstractmethod
    def train(self, training_functions, matching_functions) -> TrainingResult:
        """

        :param training_functions:
        :param matching_functions:
        :return:
        """
        pass


class ModelTrainerFactory:
    """
    Singleton Factory.
    returns a __ModelTrainerFactory object.
    """

    class __ModelTrainerFactory:
        """
        Specific ModelTrainerFactory.
        """

        def __init__(self):
            """
            Initializes the object with standard ModelTrainers
            """
            self._creators = {'regression': RegressionModelTrainer}

        def register_trainer(self, trainer_type, trainer) -> None:
            """
            Registers a new ModelTrainer.
            :param trainer_type: Type of ModelTrainer. If this t type is already
            defined, the ModelTrainer will be overwritten
            :param trainer: Type of a ModelTrainer. Inherited from ModelTrainer
            :return: None
            """
            self._creators[trainer_type] = trainer

        def create_model_trainer(self, trainer_type) -> ModelTrainer:
            """
            Adds returns a ModelTrainer of the given type
            :param trainer_type:
            :return:
            """
            trainer = self._creators.get(trainer_type)
            if not trainer:
                raise ValueError(trainer)
            return trainer()

    __instance = None

    @staticmethod
    def get_instance() -> __ModelTrainerFactory:
        """
        Singleton method for getting the ModelTrainerFactory
        :return: __ModelTrainerFactory object.
        """
        if ModelTrainerFactory.__instance is None:
            ModelTrainerFactory.__instance = ModelTrainerFactory.__ModelTrainerFactory()

        return ModelTrainerFactory.__instance


class RegressionModelTrainer(ModelTrainer):

    def __init__(self):
        self._evaluator = evaluation.LeastSquareEvaluator()

    def train(self, training_functions: pandas.DataFrame, ideal_functions: pandas.DataFrame) -> TrainingResult:
        """
        Determines the best fitting ideal functions by using training function and calculating
        least squares.
        :param training_functions: DataFrame containing the training functions
        :param ideal_functions: DataFrame containing the ideal functions
        :return:
        """
        least_squares_functions = {}
        for (column_name, column_data) in training_functions.iteritems():
            if column_name.startswith('y'):
                # Calculate best fitting function for: column_name
                least_squares_array = {}
                for (ideal_name, ideal_data) in ideal_functions.iteritems():
                    if ideal_name.startswith('y'):
                        mse = self._evaluator.evaluate(column_data, ideal_data)
                        least_squares_array[ideal_name] = mse[0], mse[1]
                least_squares_functions[column_name] = least_squares_array

        return TrainingResult(least_squares_functions)
