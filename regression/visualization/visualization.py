import abc

import pandas
from bokeh.models import ranges, ColumnDataSource, LabelSet, HoverTool
from bokeh.palettes import PuBu, Spectral6
from bokeh.plotting import figure, Figure
from bokeh.transform import factor_cmap


class Visualizer(metaclass=abc.ABCMeta):
    """
    Abstract visualizer.
    Visualizes data via bokeh library.
    """

    @abc.abstractmethod
    def visualize(self, function_name, data) -> Figure:
        """
        Visualizes data via bokeh
        :param function_name:
        :param data:
        :return: abstract - returns figure of implementation
        """
        pass


class VisualizerFactory:
    """
    Singleton Factory.
    returns a __VisualizerFactory object.
    """

    class __VisualizerFactory:
        """
        Specific VisualizerFactory.
        """

        def __init__(self):
            """
            Initializes the object with standard ModelTrainers
            """
            self._creators = {'training': TrainingVisualizer,
                              'functions': FunctionsVisualizer,
                              'test': EvaluationVisualizer}

        def register_visualizer(self, visualization_type, trainer) -> None:
            """
            Registers a new ModelTrainer.
            :param visualization_type: Type of ModelTrainer. If this t type is already
            defined, the ModelTrainer will be overwritten
            :param trainer: Type of a ModelTrainer. Inherited from ModelTrainer
            :return: None
            """
            self._creators[visualization_type] = trainer

        def create_visualizer(self, visualization_type) -> Visualizer:
            """
            Adds returns a ModelTrainer of the given type
            :param visualization_type:
            :return:
            """
            visualizer = self._creators.get(visualization_type)
            if not visualizer:
                raise ValueError(visualizer)
            return visualizer()

    __instance = None

    @staticmethod
    def get_instance() -> __VisualizerFactory:
        """
        Singleton method for getting the ModelTrainerFactory
        :return: __ModelTrainerFactory object.
        """
        if VisualizerFactory.__instance is None:
            VisualizerFactory.__instance = VisualizerFactory.__VisualizerFactory()

        return VisualizerFactory.__instance


class FunctionsVisualizer(Visualizer):
    """
    Visualizes two functions in one plot
    """

    def visualize(self, function_name, data) -> Figure:
        """
        Visualizes two functions in one plot
        :param function_name:
        :param data: pandas.Dataframe with columns: x,y_t (training function), y_i (ideal function)s
        :return: returns the figure containing the plot
        """
        datasource_first_fct = ColumnDataSource(dict(x=data['x'].values, y=data['y_t']))
        datasource_first_scd = ColumnDataSource(dict(x=data['x'].values, y=data['y_i']))

        p = figure(plot_width=750,
                   plot_height=500,
                   x_axis_label="Ideal functions",
                   y_axis_label="Sum of Least squares",
                   title="Training overview training function " + function_name,
                   x_range=ranges.DataRange1d(start=datasource_first_fct.data['x'].min(),
                                              end=datasource_first_fct.data['x'].max(),
                                              default_span=0.1),
                   y_range=ranges.Range1d(start=datasource_first_fct.data['y'].min(),
                                          end=datasource_first_fct.data['y'].max()),
                   tools=['save', 'ywheel_zoom', 'reset', 'redo', 'undo'],
                   active_scroll='ywheel_zoom')

        p.line(source=datasource_first_fct, line_width=4, line_color='red', legend_label="Training function")
        p.line(source=datasource_first_scd, line_width=2, line_color='blue', legend_label="Ideal function")

        return p


class TrainingVisualizer(Visualizer):
    """
    Visualizes the training of a regression based on the deviation of the training
    function.
    """

    def visualize(self, function_name, data) -> Figure:
        """
        Visualizes the training of a regression based on the deviation of the training
        function.
        :param function_name:
        :param data: pandas.Dataframe with colums: function_name, fitting value
        :return: returns the figure containing the plot
        """
        x_values = list(data.keys())
        y_values = []

        for value in data.values():
            y_values.append(value[0])

        hover = HoverTool(tooltips=[
            ("index", "$index"),
            ("(function name, error)", "(@x, @y)")
        ])


        datasource = ColumnDataSource(dict(x=x_values, y=y_values))

        f = figure(x_range=x_values, y_axis_type="log", plot_height=500, plot_width=750, title="Least Squares",
                   toolbar_location="below", tools=[hover, 'save', 'ywheel_zoom', 'reset', 'redo', 'undo'],
                   active_scroll='ywheel_zoom')

        f.circle(source=datasource, x='x', y='y', size=10, alpha=0.5, color=PuBu[7][2])

        labels = LabelSet(x='x', y='y', text='y', level='glyph',
                          x_offset=-13.5, y_offset=0, source=datasource, render_mode='canvas')

        f.add_layout(labels)

        return f


class EvaluationVisualizer(Visualizer):
    """
    Visualizes the testing processs
    """

    def visualize(self, function_name: str, data: pandas.DataFrame) -> Figure:

        source = ColumnDataSource(data)
        hover = HoverTool(tooltips=[
            ("index", "$index"),
            ("(x,y)", "(@{X (test func)}, @{Y (test func)})"),
            ("deviation", "@{Delta Y (test func)}"),
            ("ideal function", "@{No.  of ideal func}"),
        ])

        p = figure(title=function_name, tools=[hover, 'wheel_zoom', 'box_zoom', 'reset', 'pan'])
        p.xaxis.axis_label = 'x'
        p.yaxis.axis_label = 'y'
        p.width = 750

        mapper = factor_cmap(field_name='No.  of ideal func',
                             palette=Spectral6,
                             factors=data['No.  of ideal func'].unique())

        p.scatter(x='X (test func)',
                  y='Y (test func)',
                  radius='Delta Y (test func)',
                  color=mapper,
                  source=source,
                  legend_field="No.  of ideal func")

        p.x(x='X (test func)',
            y='Y (test func)',
            source=source,
            size=10,
            line_color='black')

        return p
