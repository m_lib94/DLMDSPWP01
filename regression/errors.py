class ModelNotTrainedError(Exception):
    """
    Generic error that indicates, that the model was not trained.
    """
    pass


class ModelNotPreparedError(Exception):
    """
    Generic error that indicates, that the model was not prepared.
    """
    pass


