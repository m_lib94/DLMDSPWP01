from pandas import DataFrame


class Tester:
    """
    Generic class for evaluating/testing a function against some key value pair
    """
    def __init__(self, threshold, comparing_function):
        """

        :param threshold:
        :param comparing_function:
        """
        self._threshold = threshold
        self._comparing_function = comparing_function

    def evaluate(self, model_functions: DataFrame, test_data: dict, max_deviations: dict):
        """

        :param max_deviations:
        :param model_functions:
        :param test_data:
        :return: returns
        """
        result = None
        test = model_functions.loc[model_functions['x'] == test_data['x']]
        # Calculate if  x and y fit criteria
        for column_name, value in test.iteritems():
            if column_name.startswith('y'):
                deviation = self._comparing_function(test_data['y'], value.values[0])
                if deviation < (max_deviations[column_name][1] * self._threshold):
                    if result is None:
                        result = (deviation, column_name)
                    else:
                        if deviation < result[0]:
                            result = (deviation, column_name)

        return result
