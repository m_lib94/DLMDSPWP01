import pandas
import math
from csv import DictReader

from bokeh.layouts import gridplot
from bokeh.io import output_file, show

from .errors import ModelNotTrainedError, ModelNotPreparedError
from .persistence import Database
from .testing import Tester
from .training import ModelTrainerFactory
from .visualization import VisualizerFactory





class Regression:
    """
    Basic class for training and testing the regression based
    on given paramaters.
    """

    def __init__(self):
        """
        Initializing the database, if object is created.
        """
        self._db = Database()
        self._training_result = None

    def prepare_data(self, training_path, ideal_path):
        """
        Prepares the data for training and evaluation process.
        Imports the data from the files given by training_path
        and ideal path
        :param training_path: path to file, that contains training data
        :param ideal_path: path to file, that contains ideal data
        :return: None
        """
        try:
            training_data = self._import_csv(training_path)
            ideal_data = self._import_csv(ideal_path)

            # persist training data
            self._db.insert_training_functions(training_data)

            # persist evaluation data
            self._db.insert_ideal_functions(ideal_data)
        except FileNotFoundError as e:
            raise Exception("Test") from e

    def train(self, visualize=False):
        """
        Determines the best fitting function for each training function,
        which was imported in self.prepare_data().
        :param visualize: Boolean Flag, if True the process is visualized
        :return: None
        """
        try:
            training_functions = self._db.get_training_functions()

            ideal_functions = self._db.get_ideal_functions()

            trainer_factory = ModelTrainerFactory.get_instance()

            trainer = trainer_factory.create_model_trainer('regression')

            self._training_result = trainer.train(training_functions, ideal_functions)

            if visualize:
                self._visualize_training(training_functions, ideal_functions)
        except Exception:
            raise ModelNotPreparedError()

    def evaluate(self, test_path, visualize=False):
        """
        Evaluates the given data as csv line by line and determines, if one of the
        best fitting ideal functions matches the the criterion.
        If a data point from the test path does not match an ideal function,
        it is NOT part of the result and as outlier out of scope.
        :param visualize: Boolean Flag, if True the process is visualized
        :param test_path
        :raise ModelNotTrainedError
        :return:
        """
        try:
            best_fitting = self._training_result.get_best_fitting()

            # Get determined best fitting functions
            fitting_functions = self._db.get_ideal_functions(best_fitting.keys())

            print(fitting_functions)

            columns = ['X (test func)', 'Y (test func)', 'Delta Y (test func)', 'No.  of ideal func']

            tester = Tester(math.sqrt(2), (lambda a, b: abs(a - b)))

            result_list = []
            # open file in read mode
            with open(test_path, 'r') as read_obj:
                csv_dict_reader = DictReader(read_obj)
                for row in csv_dict_reader:
                    result = tester.evaluate(fitting_functions, {'x': float(row['x']), 'y': float(row['y'])}, best_fitting)
                    if result is not None:
                        result_list.append([float(row['x']), float(row['y']), result[0], result[1]])

            result_df = pandas.DataFrame(result_list, columns=columns)
            self._db.insert_evaluation(result_df)

            if visualize:
                self._visualize_evaluation(result_df)
        except Exception:
            raise ModelNotTrainedError()

    def _visualize_training(self, training_functions, ideal_functions):
        """
        Visualizes the training process.
        :param training_functions:
        :param ideal_functions:
        :return: None
        """
        output_file("training.html")

        training_visualizer = VisualizerFactory.get_instance().create_visualizer("training")

        training_figures = []
        for training in self._training_result.get_all_results():
            training_figure = training_visualizer.visualize(training,
                                                            self._training_result.get_all_results()[training])
            training_figures.append(training_figure)

        fitting_functions = self._training_result.get_best_fitting()
        function_visualizer = VisualizerFactory.get_instance().create_visualizer("functions")

        function_figures = []
        for i in range(0, len(fitting_functions)):
            training = 'y' + str((i + 1))
            best_fitting = list(fitting_functions.keys())[i]
            func_data = dict(x=training_functions['x'],
                             y_t=training_functions[training].values,
                             y_i=ideal_functions[best_fitting].values
                             )
            function_figure = function_visualizer.visualize(training, func_data)
            function_figures.append(function_figure)

        grid_container = []
        for t, f in zip(training_figures, function_figures):
            grid_container.append([t, f])

        grid = gridplot(grid_container)
        show(grid)

    def _visualize_evaluation(self, data):
        """
        Visualizes nthe evaluation data as configured.
        :param data:
        :return:
        """
        output_file("result.html")
        visualizer = VisualizerFactory.get_instance().create_visualizer("test")

        plot = visualizer.visualize("Testing process", data)

        show(plot)

    def _import_csv(self, path) -> pandas.DataFrame:
        """
        Imports data as pandas.DataFrame
        :param path: path to csv file
        :return: DataFrame with data loaded from csv-File
        """
        result = pandas.read_csv(path)
        return result



