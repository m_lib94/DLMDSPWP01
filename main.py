"""
This is the entry point of the program.
To execute in run this file and add the three path arguments for the data
E.g. : python main.py /path/to/training.csv /path/to/ideal.csv /path/to/test.csv.

Make sure at least python version 3.8 is installed.
"""

import regression
import sys

if __name__ == '__main__':

    if len(sys.argv) == 4:
        training_path = sys.argv[1]
        ideal_path = sys.argv[2]
        test_path = sys.argv[3]

        regression = regression.Regression()

        regression.prepare_data(training_path, ideal_path)

        regression.train(visualize=True)

        regression.evaluate(test_path, visualize=True)
    else:
        raise ValueError("Not enough argmuments are used.")

